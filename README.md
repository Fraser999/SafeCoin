# SafeCoin

Example implementation of SafeCoin types.

See [the generated docs](https://fraser999.gitlab.io/SafeCoin/doc/safecoin/) for further info.
