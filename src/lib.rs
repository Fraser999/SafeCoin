//! Example implementation of safecoin types.
//!
//! # Examples
//!
//! ```
//! use safecoin::{Coins, Error, MicroCoins, MilliCoins, NanoCoins};
//! use std::{convert::TryFrom, str::FromStr};
//!
//! let c1 = Coins::from_str("1.234").unwrap();
//! let c2 = Coins::from(MilliCoins::new(1234).unwrap());
//! assert_eq!(c1, c2);
//!
//! let c3 = "0.123456789".parse::<Coins>().unwrap();
//! let c4 = Coins::from(NanoCoins::new(123_456_789).unwrap());
//! assert_eq!(c3, c4);
//!
//! let c5 = Coins::from_str("1.2345").unwrap();
//! let micro = MicroCoins::try_from(c5).unwrap();
//! assert_eq!(1_234_500, micro.num());
//!
//! // We can't accurately represent 1.2345 safecoin purely in terms of milli-safecoin
//! let milli_err = MilliCoins::try_from(c5).unwrap_err();
//! assert_eq!(Error::LossOfPrecision, milli_err);
//! ```

#![doc(test(attr(forbid(warnings))))]
#![warn(unused, missing_copy_implementations, missing_docs)]
#![deny(
    deprecated_in_future,
    future_incompatible,
    macro_use_extern_crate,
    rust_2018_idioms,
    nonstandard_style,
    single_use_lifetimes,
    trivial_casts,
    trivial_numeric_casts,
    unreachable_pub,
    unsafe_code,
    unstable_features,
    unused_import_braces,
    unused_lifetimes,
    unused_qualifications,
    unused_results,
    warnings,
    clippy::all,
    clippy::pedantic
)]
#![forbid(
    const_err,
    duplicate_macro_exports,
    exceeding_bitshifts,
    incoherent_fundamental_impls,
    invalid_type_param_default,
    legacy_constructor_visibility,
    legacy_directory_ownership,
    macro_expanded_macro_exports_accessed_by_absolute_paths,
    missing_fragment_specifier,
    mutable_transmutes,
    no_mangle_const_items,
    order_dependent_trait_objects,
    overflowing_literals,
    parenthesized_params_in_types_and_modules,
    pub_use_of_private_extern_crate,
    safe_extern_statics,
    unknown_crate_types
)]
#![allow(unreachable_pub)]

// TODO:
//   * Arithmetic operations for all types
//   * Document
//   * Test
//   * Improve documentation of how to construct and how that can fail

mod coins;
mod error;

pub use coins::{Coins, MicroCoins, MilliCoins, NanoCoins, MAX_COINS_VALUE};
pub use error::{Error, Result};
