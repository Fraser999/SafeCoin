use crate::{Error, Result};
use std::{
    convert::TryFrom,
    fmt::{self, Debug, Display, Formatter},
    str::FromStr,
};
use unwrap::unwrap;

/// The maximum amount of safecoin represented by a single `Coins`.
pub const MAX_COINS_VALUE: Coins = Coins(((u32::max_value() as u64) << 32) + PARTS_FACTOR - 1);
const PARTS_FACTOR: u64 = 4_000_000_000;

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
/// Structure representing a safecoin amount.
///
/// See [the module-level documentation](index.html) for examples.
pub struct Coins(u64);

impl Coins {
    #[doc(hidden)]
    pub fn from_raw(value: u64) -> Result<Self> {
        if value >= MAX_COINS_VALUE.0 {
            return Err(Error::ExcessiveValue);
        }
        Ok(Self(value))
    }

    /// The maximum value a `Coins` can represent.
    pub fn max_value() -> Self {
        MAX_COINS_VALUE
    }
}

impl FromStr for Coins {
    type Err = Error;

    fn from_str(value_str: &str) -> Result<Self> {
        let mut itr = value_str.splitn(2, '.');
        let units = itr
            .next()
            .and_then(|s| s.parse::<u64>().ok())
            .ok_or(Error::FailedToParse)?;
        let _ = u32::try_from(units).map_err(|_| Error::ExcessiveValue)?;

        let remainder_str = itr.next().unwrap_or_default().trim_end_matches('0');
        let remainder = if remainder_str.is_empty() {
            0
        } else {
            remainder_str
                .parse::<u64>()
                .map_err(|_| Error::FailedToParse)?
        };

        let remainder_as_pico = if remainder_str.len() <= 12 {
            remainder * 10_u64.pow(12 - unwrap!(u32::try_from(remainder_str.len())))
        } else {
            return Err(Error::LossOfPrecision);
        };
        let parts = if remainder_as_pico % 250 == 0 {
            remainder_as_pico / 250
        } else {
            return Err(Error::LossOfPrecision);
        };

        Ok(Self((units << 32) + parts))
    }
}

impl Debug for Coins {
    #[allow(trivial_casts)]
    fn fmt(&self, formatter: &mut Formatter<'_>) -> fmt::Result {
        (self as &dyn Display).fmt(formatter)
    }
}

impl Display for Coins {
    fn fmt(&self, formatter: &mut Formatter<'_>) -> fmt::Result {
        write!(
            formatter,
            "Coins({}.{})",
            self.0 >> 32,
            format!("{:012}", ((self.0 << 32) >> 32) * 250).trim_end_matches('0')
        )
    }
}

macro_rules! sub_coin {
    ($name:ident, $doc_name:expr, $divisor:expr) => {
        sub_coin!($name, stringify!($name), $doc_name, $divisor);
    };
    ($name:ident, $type_name:expr, $doc_name:expr, $divisor:expr) => {
        #[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
        #[allow(clippy::module_name_repetitions)]
        #[doc = "Structure representing a quantity of "]
        #[doc = $doc_name]
        #[doc = ""]
        #[doc = "See [the module-level documentation](index.html) for examples."]
        pub struct $name {
            num: u64,
        }

        impl $name {
            #[doc = "Construct a new `"]
            #[doc = $type_name]
            #[doc = "`.  Returns an error if `num > Self::max_num()`."]
            pub fn new(num: u64) -> Result<Self> {
                if num > Self::max_num() {
                    return Err(Error::ExcessiveValue);
                }
                Ok(Self { num })
            }

            #[doc = "The maximum number this `"]
            #[doc = $type_name]
            #[doc = "` can hold.  This is the greatest safecoin value which represents less than or
                     equal to `MAX_COINS_VALUE`."]
            pub fn max_num() -> u64 {
                (1_u64 << 32) * Self::divisor() - 1
            }

            #[doc = "The number of "]
            #[doc = $doc_name]
            #[doc = "represented by the `"]
            #[doc = $type_name]
            #[doc = "`."]
            pub fn num(&self) -> u64 {
                self.num
            }

            #[doc = "The divisor of a single safecoin which this `"]
            #[doc = $type_name]
            #[doc = "` represents."]
            fn divisor() -> u64 {
                $divisor
            }
        }

        impl From<$name> for Coins {
            fn from(subcoin: $name) -> Self {
                let units = subcoin.num() / $name::divisor();
                let parts =
                    PARTS_FACTOR / $name::divisor() * (subcoin.num() % $name::divisor());
                Self((units << 32) + parts)
            }
        }

        #[doc = "Try to convert a `Coins` value to the equivalent `"]
        #[doc = $type_name]
        #[doc = "` value.  Will return an error if this conversion would incur a loss of precision,
                 e.g. if the `Coins` represents 250 pico-safecoins."]
        impl TryFrom<Coins> for $name {
            type Error = Error;
            fn try_from(coin: Coins) -> Result<Self> {
                let picos = ((coin.0 << 32) >> 32) * 250;
                if picos % (1_000_000_000_000 / Self::divisor()) != 0 {
                    return Err(Error::LossOfPrecision);
                }
                let mut num = picos / (1_000_000_000_000 / Self::divisor());
                num += (coin.0 >> 32) * Self::divisor();
                Ok(Self { num })
            }
        }
    };
}

sub_coin!(MilliCoins, "milli-safecoins", 1_000);
sub_coin!(MicroCoins, "micro-safecoins", 1_000_000);
sub_coin!(NanoCoins, "nano-safecoins", 1_000_000_000);

#[cfg(test)]
#[allow(clippy::cast_possible_truncation)]
mod tests {
    use super::*;
    use std::convert::TryInto;

    fn check_coin(expected_units: u32, expected_parts: u32, coin: Coins) {
        assert_eq!(expected_units, (coin.0 >> 32) as u32);
        assert_eq!(expected_parts, ((coin.0 << 32) >> 32) as u32);
    }

    #[test]
    fn from_str() {
        check_coin(0, 0, unwrap!(Coins::from_str("0")));
        check_coin(0, 0, unwrap!(Coins::from_str("0.0")));
        check_coin(0, 1, unwrap!(Coins::from_str("0.00000000025")));
        check_coin(1, 1, unwrap!(Coins::from_str("1.00000000025")));
        check_coin(1, PARTS_FACTOR as u32 / 10, unwrap!(Coins::from_str("1.1")));
        check_coin(
            1,
            PARTS_FACTOR as u32 / 10 + 1,
            unwrap!(Coins::from_str("1.10000000025")),
        );
        check_coin(u32::max_value(), 0, unwrap!(Coins::from_str("4294967295")));
        check_coin(
            u32::max_value(),
            PARTS_FACTOR as u32 - 1,
            unwrap!(Coins::from_str("4294967295.99999999975")),
        );
        check_coin(
            u32::max_value(),
            PARTS_FACTOR as u32 - 1,
            unwrap!(Coins::from_str("4294967295.999999999750000")),
        );
        assert_eq!(Err(Error::FailedToParse), Coins::from_str("a"));
        assert_eq!(Err(Error::FailedToParse), Coins::from_str("0.a"));
        assert_eq!(Err(Error::FailedToParse), Coins::from_str("0.0.0"));
        assert_eq!(
            Err(Error::LossOfPrecision),
            Coins::from_str("0.00000000024")
        );
        assert_eq!(
            Err(Error::LossOfPrecision),
            Coins::from_str("0.00000000026")
        );
        assert_eq!(
            Err(Error::LossOfPrecision),
            Coins::from_str("0.10000000024")
        );
        assert_eq!(
            Err(Error::LossOfPrecision),
            Coins::from_str("0.10000000026")
        );
        assert_eq!(Err(Error::ExcessiveValue), Coins::from_str("4294967296"));
    }

    #[test]
    fn milli_coins() {
        check_coin(0, 0, Coins::from(unwrap!(MilliCoins::new(0))));
        check_coin(
            0,
            PARTS_FACTOR as u32 / 1000,
            Coins::from(unwrap!(MilliCoins::new(1))),
        );
        check_coin(
            0,
            PARTS_FACTOR as u32 / 100,
            Coins::from(unwrap!(MilliCoins::new(10))),
        );
        check_coin(1, 0, Coins::from(unwrap!(MilliCoins::new(1000))));
        check_coin(
            u32::max_value(),
            0,
            Coins::from(unwrap!(MilliCoins::new(4_294_967_295_000))),
        );
        check_coin(
            u32::max_value(),
            (PARTS_FACTOR - PARTS_FACTOR / 1000) as u32,
            Coins::from(unwrap!(MilliCoins::new(4_294_967_295_999))),
        );
        assert_eq!(
            Err(Error::ExcessiveValue),
            MilliCoins::new(4_294_967_296_000)
        );
    }

    #[test]
    fn micro_coins() {
        check_coin(0, 0, Coins::from(unwrap!(MicroCoins::new(0))));
        check_coin(
            0,
            PARTS_FACTOR as u32 / 1_000_000,
            Coins::from(unwrap!(MicroCoins::new(1))),
        );
        check_coin(
            0,
            PARTS_FACTOR as u32 / 100_000,
            Coins::from(unwrap!(MicroCoins::new(10))),
        );
        check_coin(1, 0, Coins::from(unwrap!(MicroCoins::new(1_000_000))));
        check_coin(
            u32::max_value(),
            0,
            Coins::from(unwrap!(MicroCoins::new(4_294_967_295_000_000))),
        );
        check_coin(
            u32::max_value(),
            (PARTS_FACTOR - PARTS_FACTOR / 1_000_000) as u32,
            Coins::from(unwrap!(MicroCoins::new(4_294_967_295_999_999))),
        );
        assert_eq!(
            Err(Error::ExcessiveValue),
            MicroCoins::new(4_294_967_296_000_000)
        );
    }

    #[test]
    fn nano_coins() {
        check_coin(0, 0, Coins::from(unwrap!(NanoCoins::new(0))));
        check_coin(
            0,
            PARTS_FACTOR as u32 / 1_000_000_000,
            Coins::from(unwrap!(NanoCoins::new(1))),
        );
        check_coin(
            0,
            PARTS_FACTOR as u32 / 100_000_000,
            Coins::from(unwrap!(NanoCoins::new(10))),
        );
        check_coin(1, 0, Coins::from(unwrap!(NanoCoins::new(1_000_000_000))));
        check_coin(
            u32::max_value(),
            0,
            Coins::from(unwrap!(NanoCoins::new(4_294_967_295_000_000_000))),
        );
        check_coin(
            u32::max_value(),
            (PARTS_FACTOR - PARTS_FACTOR / 1_000_000_000) as u32,
            Coins::from(unwrap!(NanoCoins::new(4_294_967_295_999_999_999))),
        );
        assert_eq!(
            Err(Error::ExcessiveValue),
            NanoCoins::new(4_294_967_296_000_000_000)
        );
    }

    #[test]
    #[allow(clippy::cognitive_complexity)]
    fn to_subcoins() {
        let c = unwrap!(Coins::from_str("0"));
        assert_eq!(MilliCoins::new(0), c.try_into());
        assert_eq!(MicroCoins::new(0), c.try_into());
        assert_eq!(NanoCoins::new(0), c.try_into());

        let c = unwrap!(Coins::from_str("0.001"));
        assert_eq!(MilliCoins::new(1), c.try_into());
        assert_eq!(MicroCoins::new(1_000), c.try_into());
        assert_eq!(NanoCoins::new(1_000_000), c.try_into());

        let c = unwrap!(Coins::from_str("0.000001"));
        assert_eq!(Err(Error::LossOfPrecision), MilliCoins::try_from(c));
        assert_eq!(MicroCoins::new(1), c.try_into());
        assert_eq!(NanoCoins::new(1_000), c.try_into());

        let c = unwrap!(Coins::from_str("0.000000001"));
        assert_eq!(Err(Error::LossOfPrecision), MilliCoins::try_from(c));
        assert_eq!(Err(Error::LossOfPrecision), MicroCoins::try_from(c));
        assert_eq!(NanoCoins::new(1), c.try_into());

        let c = unwrap!(Coins::from_str("0.00000000025"));
        assert_eq!(Err(Error::LossOfPrecision), MilliCoins::try_from(c));
        assert_eq!(Err(Error::LossOfPrecision), MicroCoins::try_from(c));
        assert_eq!(Err(Error::LossOfPrecision), NanoCoins::try_from(c));

        assert_eq!(
            Err(Error::LossOfPrecision),
            MilliCoins::try_from(MAX_COINS_VALUE)
        );
        assert_eq!(
            Err(Error::LossOfPrecision),
            MicroCoins::try_from(MAX_COINS_VALUE)
        );
        assert_eq!(
            Err(Error::LossOfPrecision),
            NanoCoins::try_from(MAX_COINS_VALUE)
        );

        let c = unwrap!(Coins::from_str("4294967295.001"));
        assert_eq!(MilliCoins::new(4_294_967_295_001), c.try_into());
        assert_eq!(MicroCoins::new(4_294_967_295_001_000), c.try_into());
        assert_eq!(NanoCoins::new(4_294_967_295_001_000_000), c.try_into());

        let c = unwrap!(Coins::from_str("4294967295.000001"));
        assert_eq!(Err(Error::LossOfPrecision), MilliCoins::try_from(c));
        assert_eq!(MicroCoins::new(4_294_967_295_000_001), c.try_into());
        assert_eq!(NanoCoins::new(4_294_967_295_000_001_000), c.try_into());

        let c = unwrap!(Coins::from_str("4294967295.000000001"));
        assert_eq!(Err(Error::LossOfPrecision), MilliCoins::try_from(c));
        assert_eq!(Err(Error::LossOfPrecision), MicroCoins::try_from(c));
        assert_eq!(NanoCoins::new(4_294_967_295_000_000_001), c.try_into());

        let c = unwrap!(Coins::from_str("4294967295.00000000025"));
        assert_eq!(Err(Error::LossOfPrecision), MilliCoins::try_from(c));
        assert_eq!(Err(Error::LossOfPrecision), MicroCoins::try_from(c));
        assert_eq!(Err(Error::LossOfPrecision), NanoCoins::try_from(c));
    }
}
