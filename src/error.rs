use std::result;

/// A specialised `Result` type for safecoin.
pub type Result<T> = result::Result<T, Error>;

/// Safecoin error variants.
#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Error {
    /// While parsing or converting, precision would be lost.
    LossOfPrecision,
    /// The safecoin amount would exceed
    /// [the maximum value for `Coin`](constant.MAX_COINS_VALUE.html).
    ExcessiveValue,
    /// Failed to parse the string as a `Coin`.
    FailedToParse,
}
